﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _210819
{
    class Program
    {
        static void Main(string[] args)
        {
            // teeme tabeli (2 kohalise massiivi)

            int[,] tabel = new int[10, 10]; // teeb 10 x 10 suuruse tabeli

            Random r = new Random();

            // täidame taeli juhuslike arvudega:

            for (int i = 0; i < tabel.GetLength(0); i++)
            {
                for (int k = 0; k < tabel.GetLength(1); k++)
                {
                     tabel[i, k] = r.Next(1, 101); // vahemikus 1-100, 101 on välja arvatud
                }
  
            }
            // prindime välja ja info tabeli kohta:

            foreach (var x in tabel) Console.Write("{0}, ", x);
            Console.WriteLine();
            Console.WriteLine(tabel.Length);
            Console.WriteLine(tabel.Rank);
            Console.WriteLine(tabel.GetLength(0));
            Console.WriteLine(tabel.GetLength(1));

            // prindime välja tabeli read eraldi:

            for (int i = 0; i < tabel.GetLength(0); i++)
            {
                for (int k = 0; k < tabel.GetLength(1); k++)
                {
                    Console.Write($" {tabel[i, k]}\t ");
                }
                Console.WriteLine();
            }

            // küsib ja leiab arvu asukoha:

            while (true) // siin saab ka uue bool muutujaga või for tsükliga teha, kus on defineeritud muutuja: for (bool kasKüsimeEdasi = true; kas KüsimeEdasi;)
            {
                Console.Write("Mida me otsime: ");
                if (int.TryParse(Console.ReadLine(), out int otsitav) && otsitav <= 100 && otsitav > 0) // saab ka: int otsitav = int.Parse(Console.ReadLine());
                // TtryParse proovib kas teksti saab teksti arvuks teha ja paneb ta väljund muutujasse out int (tüüp)
                {
                    for (int i = 0; i < tabel.GetLength(0); i++)
                    {
                        for (int k = 0; k < tabel.GetLength(1); k++)
                        {
                             if (tabel[i, k] == otsitav)
                             {
                                Console.WriteLine($"Leidsin otsitava {otsitav} {i+1}-reast ja {k+1}- veerust");
                             }  
                        }                
                     } 

                    break; // kui saab õige arvu siis lõpetab tsükli; võib ka kasKüsimeEdasi = false siis lõpetab esimese For tsükli
                }
                else
                {
                    Console.WriteLine("See pole miski arv või pole vahemikus 1...100"); // ja küsib uue arvu
                }
            }


        }
    }
}
